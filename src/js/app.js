'use strict';


function putPixel(ctx, x, y) {
	ctx.fillRect(x, y, 1, 1);
}

/**
 ** Построение отрезка несимметричным ЦДА
 */

var CDA = (function() {
	var canvas = document.getElementById('cda-canvas');
	var ctx = canvas.getContext('2d');
	var $CDATime = $('#cda-time');

	function setupListeners() {
		$('#cda-form').on('submit', _build);
	}

	function _build(e) {
		e.preventDefault();

		var $this = $(this);
		var data = $this.serializeArray();
		var x1 = parseInt(data[0].value);
		var y1 = parseInt(data[1].value);
		var x2 = parseInt(data[2].value);
		var y2 = parseInt(data[3].value);
		var temp = 0;

		if (x1 > x2) {
			temp = x1;
			x1 = x2;
			x2 = temp;
			temp = y1;
			y1 = y2;
			y2 = temp;
		}

		var Px = x2 - x1;
		var Py = y2 - y1;

		canvas.style.display = 'block';

		ctx.clearRect(0, 0, canvas.width, canvas.height);

		var defaultTime = performance.now();

		ctx.beginPath();
		ctx.moveTo(x1, y1);
		ctx.lineTo(x2, y2);
		ctx.stroke();

		defaultTime = performance.now() - defaultTime;

		ctx.clearRect(0, 0, canvas.width, canvas.height);

		var time = performance.now();

		putPixel(ctx, x1, y1);

		while (x1 < x2) {
			x1++;

			y1 = y1 + Py / Px;

			putPixel(ctx, x1, y1);
		}

		time = performance.now() - time;

		$CDATime.html('Время построения отрезка несимметричным ЦДА = <b>' + time + '</b><br>Время построения отрезка стандартными методами = <b>' + defaultTime + '</b>');
	}

	return {
		init: function() {
			setupListeners();
		}
	}
}());


/**
 ** Построение окружности по алгоритму Брезенхема 
 */

var Circle = (function() {
	var canvas = document.getElementById('circle-canvas');
	var ctx = canvas.getContext('2d');
	var $CircleTime = $('#circle-time');

	function setupListeners() {
		$('#circle-form').on('submit', _build);
	}

	function _build(e) {
		e.preventDefault();

		var $this = $(this);
		var data = $this.serializeArray();
		var x0 = parseInt(data[0].value);
		var y0 = parseInt(data[1].value);
		var radius = parseInt(data[2].value);

		canvas.style.display = 'block';

		ctx.clearRect(0, 0, canvas.width, canvas.height);

		var defaultTime = performance.now();

		ctx.arc(x0, y0, radius, Math.PI * 2, true);

		defaultTime = performance.now() - defaultTime;

		ctx.clearRect(0, 0, canvas.width, canvas.height);

		var time = performance.now();
		var x = 0;
		var y = radius;
		var delta = 1 - 2 * radius;
		var error = 0;

		while (y >= 0) {
			putPixel(ctx, x0 + x, y0 + y);
			putPixel(ctx, x0 + x, y0 - y);
			putPixel(ctx, x0 - x, y0 + y);
			putPixel(ctx, x0 - x, y0 - y);

			error = 2 * (delta + y) - 1;

			if (delta < 0 && error <= 0) {
				++x;

				delta += 2 * x + 1;

				continue;
			}

			error = 2 * (delta - x) - 1;

			if (delta > 0 && error > 0) {
				--y;

				delta += 1 - 2 * y;

				continue;
			}

			++x;

			delta += 2 * (x - y);

			--y;
		}

		time = performance.now() - time;

		$CircleTime.html('Время построения окружности по алгоритму Брезенхема = <b>' + time + '</b><br>Время построения окружности стандартными методами = <b>' + defaultTime + '</b>');
	}

	return {
		init: function() {
			setupListeners();
		}
	}
}());