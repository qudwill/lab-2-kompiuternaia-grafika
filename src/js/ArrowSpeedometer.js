'use strict';

var ArrowSpeedometer = (function() {
	var canvas = document.getElementById('speedometer-canvas');
	var ctx = canvas.getContext('2d');
	var centerX = 150;
	var centerY = 200;
	var defaultArrowLength = 100;
	var arrowLength = defaultArrowLength;
	var interval;
	var $submitAcceleration = $('#submit-acceleration');
	var $speedometerTime = $('#speedometer-time');

	function setupListeners() {
		$('#generate').on('click', _generateSpeedometer);
		$('#increase-size').on('click', _increaseSize);
		$('#decrease-size').on('click', _decreaseSize);
		$('#set-value').on('submit', _setSpeedValue);
		$('#set-acceleration').on('submit', _setAcceleration);
		$('#reset').on('click', _reset);
	}

	function _generateSpeedometer(e, x, y) {
		var xValue = x || 0;
		var yValue = y || 0;

		canvas.style.display = 'block';

		ctx.clearRect(0, 0, canvas.width, canvas.height);
		ctx.beginPath();

		_renderArrow(xValue, yValue, arrowLength);
		_renderNumbers(0 - arrowLength, 0, arrowLength);
	}

	function _renderArrow(x, y, length) {
		var defaultTime = performance.now();

		ctx.moveTo(centerX, centerY);
		ctx.lineTo(x + centerX - length, y + centerY);
		ctx.stroke();
		ctx.clearRect(0, 0, canvas.width, canvas.height);
	
		defaultTime = performance.now() - defaultTime;

		var time = performance.now();
		var x2 = centerX;
		var y2 = centerY;
		var x1 = x + centerX - length;
		var y1 = y + centerY;
		var temp = 0;

		if (x1 > x2) {
			temp = x1;
			x1 = x2;
			x2 = temp;
			temp = y1;
			y1 = y2;
			y2 = temp;
		}

		var Px = x2 - x1;
		var Py = y2 - y1;

		putPixel(ctx, x1, y1);

		while (x1 < x2) {
			x1++;

			y1 = y1 + Py / Px;

			putPixel(ctx, x1, y1);
		}

		time = performance.now() - time;

		$speedometerTime.html('Время построения стрелки спидометра несимметричным ЦДА = <b>' + time + '</b><br>Время построения стандартными методами = <b>' + defaultTime + '</b>');
	}

	function _renderNumbers(x, y, length) {
		var step = 20;

		ctx.textAlign = 'center';
		ctx.textBaseline = 'bottom';
		
		for (var i = 0; i <= 180; i += step) {
			ctx.fillText(i, x + centerX, y + centerY);

			var newCoords = _getNewCoords(x, y, (step * Math.PI / 180));

			x = newCoords[0];
			y = newCoords[1];
		}
	}

	function _getNewCoords(x, y, alpha) {
		var a = x * Math.cos(alpha) - y * Math.sin(alpha);
		var b = x * Math.sin(alpha) + y * Math.cos(alpha);

		return [a, b];
	}

	function _increaseSize() {
		arrowLength += 10;

		_generateSpeedometer();
	}

	function _decreaseSize() {
		arrowLength -= 10;

		_generateSpeedometer();
	}

	function _setSpeedValue(e, x, y) {
		e ? e.preventDefault() : '';

		var xValue = x || 0;
		var yValue = y || 0;
		var speedValue = parseInt($('#speed').val());
		var newCoords = _getNewCoords(xValue - arrowLength, yValue, (speedValue * Math.PI / 180));

		_generateSpeedometer(null, newCoords[0] + arrowLength, newCoords[1]);
	}

	function _setAcceleration(e) {
		e.preventDefault();

		var seconds = parseInt($('#seconds').val());
		var x = -arrowLength;
		var y = 0;

		$submitAcceleration.attr('disabled', true);

		interval = setInterval(function() {
			var newCoords = _getNewCoords(x, y, (2 / seconds * Math.PI / 180));

			x = newCoords[0];
			y = newCoords[1];

			_generateSpeedometer(null, x + arrowLength, y);

			if (x > 99) {
				_reset();

				return false;
			}
		}, 10);
	}

	function _reset() {
		arrowLength = defaultArrowLength;

		clearInterval(interval);
		_generateSpeedometer();

		$submitAcceleration.attr('disabled', false);
	}

	return {
		init: function() {
			setupListeners();
		}
	}
}());